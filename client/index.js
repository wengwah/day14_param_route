(function() {
	var IMDBApp = angular.module("IMDBApp", []);

	var IMDBSvc = function($http, $q) {
		var imdbSvc = this;

		// GET /film/filmId
		imdbSvc.findFilmByFilmId = function(filmId) {
			var defer = $q.defer();
			$http.get("/film/" + filmId)
				.then(function(result) {
					defer.resolve(result.data);
				}).catch(function(err) {
					defer.reject(err);
				});
			return (defer.promise);
		}
	}

	var IMDBCtrl = function(IMDBSvc) {
		var imdbCtrl = this;
		imdbCtrl.filmId = null;
		imdbCtrl.result = {};
		imdbCtrl.search = function() {
			console.log("film id = ", imdbCtrl.filmId);
			IMDBSvc.findFilmByFilmId(imdbCtrl.filmId)
				.then(function(result) {
					imdbCtrl.result = result;
				}).catch(function(err) {
					console.log(">>> err = ", err);
				});

		}
	}

	IMDBSvc.$inject = [ "$http", "$q" ];
	IMDBCtrl.$inject = [ "IMDBSvc" ];

	IMDBApp.service("IMDBSvc", IMDBSvc);
	IMDBApp.controller("IMDBCtrl", IMDBCtrl);

	//IMDBApp.service("IMDBSvc", ["$http", "$q", IMDBSvc]);
	//IMDBApp.controller("IMDBCtrl", ["IMDBSvc", IMDBCtrl]);
})();
